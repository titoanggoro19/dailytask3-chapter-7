import { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import "./index.css";

function App() {
  const [count, setCount] = useState(0);
  const [string, setString] = useState("???");

  return (
    <div className="App">
      <header className="App-header">
        <div className="row">
          <div className="col-12">
            <div>
              <h4>Total Click</h4>
              <h1>{count}</h1>
              <button
                className="btn btn-success mx-3 App-button"
                onClick={() => setCount(count + 1)}
              >
                Increment
              </button>
              <button
                className="btn btn-warning mx-3 App-button"
                onClick={() => setCount(0)}
              >
                Reset
              </button>
              <button
                className="btn btn-danger mx-3 App-button"
                onClick={() => setCount(count - 1)}
              >
                Decrement
              </button>
            </div>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-12">
            <div>
              <h4>Pilih yang mana !!!</h4>
              <h2 className="p-1">Kamu pilih, {string}</h2>
              <button
                className="btn btn-success mx-3 App-button"
                onClick={() => setString("Perawan")}
              >
                Perawan
              </button>
              <button
                className="btn btn-warning mx-3 App-button"
                onClick={() => setString("???")}
              >
                Reset
              </button>
              <button
                className="btn btn-danger mx-3 App-button"
                onClick={() => setString("Janda")}
              >
                Janda
              </button>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
